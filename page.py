class Page:
    def __init__(self, page_idx,
                 question_text_css, question_text_css_results_idx,
                 radio_button_css, radio_button_answer_text,
                 next_button_css
                 ):
        self.page_idx = page_idx
        self.question_text_css = question_text_css
        self.question_text_css_results_idx = question_text_css_results_idx
        self.radio_button_css = radio_button_css
        self.radio_button_answer_text = radio_button_answer_text
        self.next_button_css = next_button_css