import schedule
import time
import datetime

from browser_wrapper import BrowserWrapper

class Scheduler:
    def __init__(self):
        self.run_cnt = 0
        self.debug = True

    def set_debug(self, debug):
        self.debug = debug

    def make_browser_wrapper(self, headless = False):
        dhc_url = 'https://yalesurvey.ca1.qualtrics.com/jfe/form/SV_eFnltn7gS5xctc9?Type=checkin'
        cas_url_domain = 'https://secure.its.yale.edu/cas/login'
        question0 = 'Will you be coming to campus today?'
        question1 = 'Are you experiencing any of the above symptoms?'
        confirmation_text = 'Thank you for completing the survey.'

        self.browser_wrapper = BrowserWrapper(headless = headless)
        self.browser_wrapper.set_debug(self.debug)

        self.browser_wrapper.set_dhc_url(dhc_url)
        self.browser_wrapper.set_cas_url_domain(cas_url_domain)
        self.browser_wrapper.set_questions([question0, question1])
        self.browser_wrapper.set_confirmation_text(confirmation_text)
        self.browser_wrapper.set_pages([self.browser_wrapper.make_page0(), self.browser_wrapper.make_page1()])

    def _run_browser_wrapper(self):
        print(f'\nRun #{self.run_cnt} at {datetime.datetime.now()} =======================\n')

        while True:
            if all([self.browser_wrapper.do_cas_page(),
                    self.browser_wrapper.fill_page(self.browser_wrapper.get_page(0)),
                    self.browser_wrapper.fill_page(self.browser_wrapper.get_page(1)),
                    self.browser_wrapper.confirm_submission()]):
                break

            print('Automatically trying again in 5 seconds')
            time.sleep(5)

        self.run_cnt += 1

    def _make_browser_wrapper_from_cas_page(self):
        # If we run n times, the first time has to be UI, and the rest are headless

        # Open CAS page
        self.browser_wrapper.do_cas_page()
        self.browser_wrapper.open(self.browser_wrapper.get_cas_url_domain())
        cookies = self.browser_wrapper.get_cookies()

        # Quit the older browser
        self.browser_wrapper.quit()

        # Create a new browser, and load the CAS auth cookies into it
        self.make_browser_wrapper(headless = True)
        self.browser_wrapper.open(self.browser_wrapper.get_cas_url_domain())
        self.browser_wrapper.add_cookies(cookies)
        self.browser_wrapper.open(self.browser_wrapper.get_dhc_url())

        if self.debug:
            print(f'Using CAS auth cookies: {cookies}')

    # Helpful function for testing: just use a higher n
    def run_n_times(self, n = 1, sleep_seconds = 0):
        self._make_browser_wrapper_from_cas_page()

        for i in range(n):
            self._run_browser_wrapper()
            time.sleep(sleep_seconds)

    def run(self, time_of_day = '8:00'):
        def _run_browser_wrapper_with_sleep_notif():
            self._run_browser_wrapper()
            print(f'Sleeping until {time_of_day}')

        self._make_browser_wrapper_from_cas_page()

        schedule.every().day.at(time_of_day.zfill(5)).do(_run_browser_wrapper_with_sleep_notif)
        print(f'Sleeping until {time_of_day}')

        while True:
            schedule.run_pending()
            time.sleep(1)

if __name__ == '__main__':
    # We don't want to close the bot's browser tab unless necessary
    # The CAS session cookie "TGC" doesn't expire until the browser session ends
    # So, to avoid having to require the user to auth every time, we keep the browser session alive
    # for as long as possible
    scheduler = Scheduler()

    while True:
        print('Show debug messages? (y/n)')
        debug = input().strip().lower()

        if debug in ['y', 'yes', 'yep']:
            scheduler.set_debug(True)
            break
        elif debug in ['n', 'no', 'nope', 'nah']:
            scheduler.set_debug(False)
            break
        else:
            print('Invalid input. Try again.')

    while True:
        print('This bot will fill out the DHC once immediately. After that, when do you want the scheduler to start every day?\n'
              'Answer either "never" (quit and do not run daily), "default" (run at 8:00am daily), or a 24-hr time — e.g. "13:30" (run at 1:30pm daily).')
        when = input().strip().lower()

        if when in ['no', 'none', 'never', 'asap', 'now']:
            scheduler.make_browser_wrapper()
            scheduler.run_n_times(n = 1)
            break
        elif when == 'default':
            scheduler.make_browser_wrapper()
            scheduler.run(time_of_day = '8:00')
            break
        else:
            try:
                scheduler.make_browser_wrapper()
                scheduler.run(time_of_day = when)
                break
            except:
                print('Invalid input. Try again.')
