import time

from splinter import Browser # docs: https://splinter.readthedocs.io/en/latest/browser.html
# Also requires: $ brew install geckodriver

from page import Page

class BrowserWrapper:
    def __init__(self, headless = False):
        self._reset_browser(headless = headless)

    def _reset_browser(self, headless = False):
        self.browser = Browser('firefox', headless = headless)

    def _try_click(self, button, success_message = None, failure_message = None):
        try:
            button.click()
            if self.debug and success_message:
                print(success_message)
            return True
        except:
            if failure_message:
                print(failure_message)
            return False

    def set_dhc_url(self, dhc_url = 'https://yalesurvey.ca1.qualtrics.com/jfe/form/SV_eFnltn7gS5xctc9?Type=checkin'):
        self.dhc_url = dhc_url

    def set_cas_url_domain(self, cas_url_domain = 'https://secure.its.yale.edu/cas/login'):
        self.cas_url_domain = cas_url_domain

    def set_questions(self, questions):
        self.questions = questions

    def set_confirmation_text(self, confirmation_text):
        self.confirmation_text = confirmation_text

    def set_pages(self, pages):
        self.pages = pages

    def set_debug(self, debug):
        self.debug = debug

    def get_page(self, page_idx):
        return self.pages[page_idx]

    def get_cas_url_domain(self):
        return self.cas_url_domain

    def get_cookies(self):
        return self.browser.cookies.all()

    def get_dhc_url(self):
        return self.dhc_url

    def add_cookies(self, cookies):
        self.browser.cookies.add(cookies)

    def open(self, url):
        self.url = url
        self.browser.visit(self.url)

    def quit(self):
        self.browser.quit()

    def do_cas_page(self):
        self.open(self.dhc_url)

        # Give it a few seconds to redirect to CAS auth
        time.sleep(2)

        i = 0
        while self.browser.url.startswith(self.cas_url_domain):
            # We're in CAS auth
            # We don't leave until user manually enters their details
            if self.debug and i == 0:
                print(f'Waiting for user to pass CAS auth')
                i += 1

            time.sleep(1)

        if self.debug:
            print(f'Passed CAS auth')

        return True

    def fill_page(self, page):
        try:
            question_text = self.browser.find_by_css(page.question_text_css)[page.question_text_css_results_idx].text.split(' *')[0]
            if not (self.browser.url == self.dhc_url and question_text == self.questions[page.page_idx]):
                print(f'Failed to load page {page.page_idx} of the DHC. Try again.')
                return False
        except:
            print(f'Failed to load page {page.page_idx} of the DHC. Try again.')
            return False

        if self.debug:
            print(f'Loaded page {page.page_idx} of the DHC: "{question_text}"')

        # Click the radio button
        radio_button = self.browser.find_by_css(page.radio_button_css)
        success_message = f'Answered the question on page {page.page_idx}: {page.radio_button_answer_text}' if self.debug else None
        failure_message = f'Failed to answer the question on page {page.page_idx}: {page.radio_button_answer_text}'
        if not self._try_click(radio_button, success_message, failure_message):
            return False

        # Click the "Next" button
        radio_button = self.browser.find_by_css(page.next_button_css)
        success_message = f'Clicked the "Next" button' if self.debug else None
        failure_message = f'Failed to click the "Next button'
        if not self._try_click(radio_button, success_message, failure_message):
            return False

        return True

    def make_page0(self):
        page_idx = 0
        question_text_css = 'div[class="QuestionText BorderColor"]'
        question_text_css_results_idx = 1
        radio_button_css = 'label[id="QID1-3-label"]'
        radio_button_answer_text = 'Yes, I will be coming to campus today'
        next_button_css = 'input[id="NextButton"]'

        page = Page(page_idx,
                    question_text_css, question_text_css_results_idx,
                    radio_button_css, radio_button_answer_text,
                    next_button_css)

        return page

    def make_page1(self):
        page_idx = 1
        question_text_css = 'div[class="QuestionText BorderColor"] p'
        question_text_css_results_idx = -1
        radio_button_css = 'label[id="QID19-2-label"]'
        radio_button_answer_text = 'No, I am not experiencing any of the above symptoms'
        next_button_css = 'input[id="NextButton"]'

        page = Page(page_idx,
                    question_text_css, question_text_css_results_idx,
                    radio_button_css, radio_button_answer_text,
                    next_button_css)

        return page

    def confirm_submission(self):
        time.sleep(2)

        try:
            confirmation_text = self.browser.find_by_css('div[id="EndOfSurvey"] p')[0].text
            if not (self.browser.url == self.dhc_url and confirmation_text == self.confirmation_text):
                print(f'Failed to submit the DHC :( Try again.')
                return False
        except:
            print(f'Failed to submit the DHC :( Try again.')
            return False

        print(f'Successfully submitted the DHC!')
        return True

    def reset(self, url = None):
        if url:
            self.open(url)
        else:
            self.open(self.dhc_url)

if __name__ == '__main__':
    dhc_url = 'https://yalesurvey.ca1.qualtrics.com/jfe/form/SV_eFnltn7gS5xctc9?Type=checkin'
    cas_url_domain = 'https://secure.its.yale.edu/cas/'
    question0 = 'Will you be coming to campus today?'
    question1 = 'Are you experiencing any of the above symptoms?'
    confirmation_text = 'Thank you for completing the survey.'

    browser_wrapper = BrowserWrapper()

    browser_wrapper.set_dhc_url(dhc_url)
    browser_wrapper.set_cas_url_domain(cas_url_domain)
    browser_wrapper.set_questions([question0, question1])
    browser_wrapper.set_confirmation_text(confirmation_text)
    browser_wrapper.set_debug(True)

    browser_wrapper.set_pages([browser_wrapper.make_page0(), browser_wrapper.make_page1()])

    while True:
        if all([browser_wrapper.do_cas_page(),
                browser_wrapper.fill_page(browser_wrapper.get_page(0)),
                browser_wrapper.fill_page(browser_wrapper.get_page(1)),
                browser_wrapper.confirm_submission()]):
            break
        print('Automatically trying again in 5 seconds')
        time.sleep(5)
