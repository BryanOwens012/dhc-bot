# DHC Bot

This command-line bot for Intel-based Macs completes the Yale Daily Health Check (DHC) for you every day, at a certain time of day that you specify.

So far in the development of this program, you must interact with the command line. Therefore, this program might not be user-friendly for people without a programming background. But it's worth a try!

Note: You must keep your computer running all the time (including possibly overnight), as the bot quits or behaves erratically if you sleep or shut down your computer.

Requirements: Mac with a 64-bit Intel CPU, which is most Macs

## Installation

There are two options: 1) using the installer and 2) installing from source code. If you have no programming background, use 1) the installer.

### Installing using the installer (for those without programming background)

If you've never installed certain software development packages before, this might take a few minutes.

1. Make sure you have the Firefox web browser installed: [download it here](https://www.mozilla.org/en-US/firefox/all/#product-desktop-release)
2. Download the installer [here](https://drive.google.com/uc?export=download&id=13rP71R68m9JXwNV3VGWW0MNPJdiRR7YG) and the bot [here](https://drive.google.com/uc?export=download&id=1016w1NOzxd09J-eQ96iuqpHyuJxzkY4J) into the same folder. Don't rename them.
3. Hover your mouse over the folder they were downloaded into. Right-click the folder. Hold down the `ALT`/`OPT` key, and click `Copy "____" as Pathname`.
4. Open the application Terminal (you can use Spotlight to search for it). Note: The `$` to the left of any line of code means that you should copy-paste that line of code (excluding the `$`) into Terminal and then press the `ENTER` key.
5. Type `$ cd PATHNAME` into Terminal, replacing `PATHNAME` with the pathname that you just copied in step 3
6. Give yourself the permission to run the installer: `$ chmod +x ./installer.sh`
7. Run the installer: `$ ./installer.sh`.
   1. When prompted "`The "xcode-select" command requires the command line developer tools. Would you like to install the tools now?`", choose `Install`. Agree to any other prompts necessary for installing.
   2. When prompted "`==> Checking for `sudo` access (which may request your password)`", enter your admin password
   3. When prompted "`Press RETURN to continue or any other key to abort`", press the `RETURN`/`ENTER` key.
   4. Wait for it to finish downloading and installing things.
8. Now, you can run the bot: `$ ./dhc-bot.sh`

After you succesfully install `dhc-bot.sh` this way, you can delete `installer.sh`.

If you'd like to run from source code instead, read the next section.

### Installing from source code (for people with a programming background)

This program uses the Python browser automation library [Splinter](https://splinter.readthedocs.io/en/latest/index.html), which is based on [Selenium](https://www.selenium.dev/) and [BeautifulSoup](https://www.crummy.com/software/BeautifulSoup/bs4/doc/) and supports interacting with JavaScript elements. The dependencies are [Geckodriver](https://github.com/mozilla/geckodriver/releases), [Splinter](https://splinter.readthedocs.io/en/latest/index.html), and [Schedule](https://pypi.org/project/schedule/).

(I generated the `dhc-bot` executable using `$ pyinstaller --onefile --windowed scheduler.py; mv scheduler dhc-bot.sh`.)

1. Make sure you have the Firefox web browser installed: [download it here](https://www.mozilla.org/en-US/firefox/all/#product-desktop-release)
2. `$ brew install geckodriver`
3. `$ pip3 install -r requirements.txt`

(Depending how you previously installed `pip`/`pip3`, your might need to call `pip` instead of `pip3`)

## Usage

If running the compiled executable, within the same Terminal window, do: `$ ./dhc-bot.sh`. If you're uncomfortable with this, you can alternatively select the `dhc-bot.sh` in Finder, right-click --> "Open With," and choose Terminal.

If running from source code: `$ python3 scheduler.py`

You'll be prompted (in the command line) to select whether you want to see debug messages and when you want the bot to complete the DHC for you every day. You can select any time of day. At this point, you might see a popup that asks `Do you want the application "dhc-bot.sh" to accept incoming network connections?` Answer `Allow`.

The bot then opens a new, visible Firefox window to ask you for your CAS credentials. Once it accepts your CAS credentials, the Firefox window will close, and the bot will take care of the rest. You never have to interact with the Firefox window or the bot again until you quit the bot. In other words, you can just hide the Terminal window, and the bot will fill out all future DHCs in the background.

By default, the bot sleeps until 8:00am (local time) daily, at which point it wakes up and completes the DHC for you, then goes back to sleep and repeats.

Leave the bot running in the background for as long as you want (like days or weeks). The bot will complete the DHC for you every day.

You can quit the bot anytime by closing the Terminal window or pressing `CTRL-C` in the Terminal window . Quitting the bot anytime yields no side effects. Restart the bot just as you would normally start the bot.

Note: You must keep your computer running all the time (including possibly overnight), as the bot quits or behaves erratically if you sleep or shut down your computer.

Note: This program might generate the file `geckodriver.log`. This file is for debugging purposes only, and you can feel free to delete it.

### DHC questions

- In response to the question "Will you be coming to campus today?", the bot answers "Yes."
- In response to the question "Are you experiencing any of the above symptoms?", the bot answers "No."

Obviously, if you actually do have any of the symptoms, you should manually log in to DHC and answer "Yes"!

## Privacy & security

**This bot does not access or store any data about you.**

**This bot does not access or store any data about your CAS credentials.**

## I would appreciate your feedback!

Contact me through Facebook Messenger or [email](mailto:bryan.owens@yale.edu)

